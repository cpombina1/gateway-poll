const os = require('os');
var LOCALPORT = 8080;
var socket = null;

var AwsProperties;
if(os.type() == "Windows_NT") {
    AwsProperties = {
        privateKey: 'certs/edc6a70b1e-private.pem.key',
        clientCert: 'certs/edc6a70b1e-certificate.pem.crt',
        caCert: 'certs/root-CA.crt',
        clientId: "Gateway",
        host: "ac32riyrbc502.iot.us-east-1.amazonaws.com",
        subscriptions: ["Status", "System", "LED", "Commands", "BeaconEcho"]
    };
} else {
    AwsProperties = {
        privateKey: '/home/pi/meshcandy-node.js-gateway/certs/edc6a70b1e-private.pem.key',
        clientCert: '/home/pi/meshcandy-node.js-gateway/certs/edc6a70b1e-certificate.pem.crt',
        caCert: '/home/pi/meshcandy-node.js-gateway/certs/root-CA.crt',
        clientId: "Gateway",
        host: "ac32riyrbc502.iot.us-east-1.amazonaws.com",
        subscriptions: ["Status", "System", "LED", "Commands", "BeaconEcho"]
    };
}


// IoT AWS
const AwsIoT = require(__dirname +'/src/awsIoT.js');
var IoT;
var gatewayList =[];

IoT = new AwsIoT(AwsProperties);
IoT.eventEmitter.on("connected", function() {
});
IoT.eventEmitter.on("error", function(error) {
    console.log(error);
});
IoT.eventEmitter.on('Status', function(cmd) {
    var check = false;
    cmd.last = new Date();
    for(var i = 0; i < gatewayList.length; i++) {
        if(cmd.name == gatewayList[i].name) {
            gatewayList[i] = cmd;
            check = true;
        }
    }
    if(!check) gatewayList.push(cmd);
   console.log(gatewayList);
   send("data", gatewayList);
});

function send(name, obj) {
    if(socket != null) socket.emit(name, obj);
}

var express = require('express'),
    app = express(),
    http = require('http').createServer(app).listen(LOCALPORT, function () {
        console.log('webserver running on port ' + LOCALPORT + '.')}),
    sys = require('util'),
    exec = require('child_process').exec,
    io = require('socket.io')(http),
    fs = require('fs');

app.use('/assets', express.static(__dirname + '/assets'));

app.get("/", function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    fs.readFile(__dirname + '/index.html', 'utf8', function (err, data) {
        res.end(data);
    });
});

io.on('connection', function (sockt) {
    socket = sockt;
    send("data", gatewayList);
    //socket.broadcast.emit('event', "Welcome user");
    socket.on('disconnect', function () {
        console.log('User disconnected');
    });
});

http.listen(8080, function () {
    console.log('listening on *:' + 8080, null);
});
