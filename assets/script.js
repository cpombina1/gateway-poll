var socket = null;
socket = io();
var current = null;
var main = document.getElementById("main");

socket.on('data', function (result) {
    main.innerHTML = '';
    var highestNumber = 0;
    for(var i = 0; i < result.length; i ++) {
        if(parseInt(result[i].name.substring(2)) > highestNumber) highestNumber = parseInt(result[i].name.substring(2), 16);
    }

    for(var i = 0; i < highestNumber; i ++) {
        var num =  (i +1).toString(16).toUpperCase();
        if(num.length == 1) num = "000" + num;
        if(num.length == 2) num = "00" + num;
        if(num.length == 3) num = "0" + num;
        var div = document.createElement('div');
        div.className = "column";
        var gNameView = document.createElement('strong');
        gNameView.innerHTML = num;
        div.append(gNameView);
        for(var j = 0; j < result.length; j ++) {
            try{
                if(result[j].name.substring(2) == num) {
                    div.style.border = "#0f0 2px solid";
                    gNameView.innerHTML = result[j].name;
                    var now = new Date();
                    var uptime = new Date (now.getTime() - (result[j].uptime * 1000));
                    var lastTime = new Date(result[j].last);
                    if((now.getTime() - lastTime.getTime()) > 60000000) div.style.border = "#ff0 2px solid";
                    var stationId = result[j].station;
                    var stationView = document.createElement('span');
                    stationView.id = stationId;
                    stationView.innerHTML = " Station: " + stationId;
                    div.append(stationView);
                    try{
                        getAccount(stationId, function(station,res, err) {
                            if(res != undefined){
                                try{
                                    var parsed = JSON.parse(res);
                                    getAccountDetails(station, parsed[0]["FK_accountId"], function(stn, data, err) {
                                        var prse = JSON.parse(data);
                                        var stnView = document.getElementById(stn);
                                        stnView.innerHTML = " Station: " + stn + " (" + prse[0].AccountName + ")";
                                    });
                                } catch (err) {

                                }
                            }
                        });
                    } catch (e) {
                    }
                    div.append(document.createElement('span'));
                    div.append(document.createElement('br'));
                    div.append(document.createElement('span').innerHTML = "Running since " + uptime.toLocaleString());
                    div.append(document.createElement('br'));
                    div.append(document.createElement('span').innerHTML = "Last ping: " + lastTime.toLocaleString());
                    div.append(document.createElement('br'));
                    var usbStat = "USB: " + result[j].usb.status;
                    div.append(document.createElement('span').innerHTML = usbStat);
                    var netStat = "";
                    for(var l = 0; l < result[j].network.length; l++) { 
                        if(result[j].network[l].device != "lo") {
                            netStat += result[j].network[l].device + " " + (result[j].network[l].active ? " UP ": " DOWN ")
                        }
                        if(l != result[j].network.length -1) netStat += "/ ";
                    }
                    div.append(document.createElement('br'));
                    div.append(document.createElement('span').innerHTML = netStat);
                    div.append(document.createElement('br'));
                    var bcnStat = "Beacon Nodes: ";
                    for(var l = 0; l < result[j].mesh.beacons.length; l++) {
                        var majmin = result[j].mesh.beacons[l].majorminor;
                        bcnStat += "(" + majmin + ")";
                        if(l != result[j].mesh.beacons.length -1) bcnStat += ", ";
                    }
                    div.append(document.createElement('span').innerHTML = bcnStat);
                    div.append(document.createElement('br'));
                    var meshStat = "Mesh Nodes: ";
                    for(var l = 0; l < result[j].mesh.mesh.length; l++) {
                        var sn = result[j].mesh.mesh[l].name;
                        meshStat += sn;
                        if(l != result[j].mesh.mesh.length -1) meshStat += ", ";
                    }
                    div.append(document.createElement('span').innerHTML = meshStat);

                }
            } catch (e) {

            }
        }
        main.append(div);
    }
    current = result;
});

socket.on('disconnect', function (result) {
    console.log("disconnected from server");
});

socket.on('connect', function (res) {
    console.log("connected to server");
});