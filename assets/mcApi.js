/*

    Functions for retrieving data from Mesh Candy API.


*/

const BASE_URL = "https://dw31g7qdsa.execute-api.us-east-1.amazonaws.com/v1";
const KEY = "rJxXd9oQCU1yZX6ZBcRay8PdR7fABSUt2xylzGng";


function getAccount(mongoId, callback) {
    var url = BASE_URL + "/account" + queryStringify({"serialNumber": mongoId});
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(mongoId, this.response, null);
    };
    
    xhr.onerror = function() {
        callback(mongoId, null, this);
      // Error code goes here.
    };
    
    //xhr.withCredentials = true;
    xhr.send();
};

function getAccountDetails(serial, mongoId, callback) {
    var url = BASE_URL + "/account" + queryStringify({"accountId": mongoId});
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(serial, this.response, null);
    };
    
    xhr.onerror = function() {
        callback(serial, null, this);
      // Error code goes here.
    };
    
    //xhr.withCredentials = true;
    xhr.send();
};

function getBattery (params, callback) {


    var url = BASE_URL + "/sensor/battery" + queryStringify(params);
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(this.response, null);
    };
    
    xhr.onerror = function() {
        callback(null, this);
      // Error code goes here.
    };
    
    //xhr.withCredentials = true;
    xhr.send();
}

function getTemperature (params, callback) {

    var url = BASE_URL + "/sensor/temperature" + queryStringify(params);
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onloadend
    xhr.onload = function() {
        callback(this.response, null);
    };
    
    xhr.onerror = function() {
        callback(null, this);
    };
    
    //xhr.withCredentials = true;
    xhr.send();
}

function getHumidity (params, callback) {

    var url = BASE_URL + "/sensor/humidity" + queryStringify(params);
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(this.response, null);
    };
    
    xhr.onerror = function() {
        callback(null, this);
    };
    
    //xhr.withCredentials = true;
    xhr.send();
}

function getPressure (params, callback) {

    var url = BASE_URL + "/sensor/pressure" + queryStringify(params);
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(this.response, null);
    };
    
    xhr.onerror = function() {
        callback(null, this);
    };
    
    //xhr.withCredentials = true;
    xhr.send();
}

function getLocation (params, callback) {

    var url = BASE_URL + "/location" + queryStringify(params);
    var method = 'GET';
    var xhr = createCORSRequest(method, url);
    xhr.setRequestHeader("X-Api-Key", KEY);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
        callback(this.response, null);
    };
    
    xhr.onerror = function() {
        callback(null, this);
      // Error code goes here.
    };
    
    //xhr.withCredentials = true;
    xhr.send();
}

//Example input [ {"tag": "asdf"}, {"accountId": 0}] output ?tag=asdf&accountId=0
var queryStringify = function (obj) {
    var uri = "?";
    for(var i = 0; i < Object.keys(obj).length; i++) {
        var key = Object.keys(obj)[i];
        var value = obj[key];
        if(value != null){
            var str = key + "=" + value;
            if(i != 0) uri += "&";
            uri += str;
        }
    }

    var res = encodeURI(uri);
    return res;
}
var createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // Most browsers.
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      // CORS not supported.
      xhr = null;
    }
    return xhr;
  };
  
  