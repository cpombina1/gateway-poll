'use-strict'

const awsIoT = require('aws-iot-device-sdk');
const events = require('events');

var AwsIoT = function (args) {
    var eventEmitter = new events.EventEmitter();
    console.log('Creating IoT Object...');
        var device = awsIoT.device({
            keyPath: args.privateKey,
            certPath: args.clientCert,
            caPath: args.caCert,
            clientId: args.clientId,
            host: args.host,
        });

    // device handlers
    device.on('connect', function() {
        eventEmitter.emit("connected");
        for(var i = 0; i < args.subscriptions.length; i++) {
            device.subscribe(args.subscriptions[i]);
        }
    });
    
    device.on('message', function(topic, payload) {
        var message = JSON.parse(payload.toString());
        /*
        console.log("messageRx:");
        console.log(topic);
        console.log(":\n\"" + message);
        console.log("\"");
        */
        eventEmitter.emit(topic, message);
    });

    device.on('error', function(error) {
        eventEmitter.emit("error", error);
    });
    function subscribe(object) {
        if(object == null || object == undefined) {
            console.log("invalid subscribe object");
            return;
        }
        device.subscribe(object);
    }

    function publishMsg(topic, msg)
        {
        device.publish(topic, msg, null, function(err) {
            if(err) {
                console.log("IoT Publish Error");
                console.log(err);
            }
        });
        }

    function publish(topic, msgType, message) {
        var msgObject = {[msgType] : message};
        var msg = JSON.stringify(msgObject);
        device.publish(topic, msg, null, function(err) {
            if(err) {
                console.log(err);
            }
        });
    };

    return {
        subscribe: subscribe,
        publish: publish,
        publishMsg: publishMsg,
        eventEmitter: eventEmitter
    };
};

module.exports = AwsIoT;





